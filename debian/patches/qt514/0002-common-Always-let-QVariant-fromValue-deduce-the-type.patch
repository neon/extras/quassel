From 41bf70c263ee0af80ad1850fabe77ffffee188f4 Mon Sep 17 00:00:00 2001
From: Manuel Nickschas <sputnick@quassel-irc.org>
Date: Tue, 7 Jan 2020 18:39:48 +0100
Subject: [PATCH 2/6] common: Always let QVariant::fromValue() deduce the type
Bug: https://bugs.quassel-irc.org/issues/1544
Bug-Ubuntu: https://bugs.launchpad.net/quassel/+bug/1885436
Origin: upstream, https://github.com/quassel/quassel/pull/518
Origin: upstream, https://github.com/quassel/quassel/commit/41bf70c263ee0af80ad1850fabe77ffffee188f4

In some places, we would explicitly give the type to
QVariant::fromValue(), forgoing automatic type deduction. This is
almost always redundant, and in the cases where it is not, the
input type should be explicitly cast.

Additionally, the implementation of QVariant::fromValue() subtly changed
in Qt 5.14, leading to compile errors when giving a non-decayed type
(as we did in SignalProxy::attachSignal()).
---
 src/client/coreaccountmodel.cpp                |  2 +-
 src/client/messagefilter.cpp                   |  2 +-
 src/client/messagemodel.cpp                    | 10 +++++-----
 src/client/networkmodel.cpp                    | 18 +++++++++---------
 src/common/buffersyncer.cpp                    |  8 ++++----
 src/common/network.cpp                         |  4 ++--
 src/common/serializers/serializers.cpp         |  2 +-
 src/common/signalproxy.h                       |  2 +-
 src/common/types.h                             | 10 +++++-----
 src/common/util.h                              |  2 +-
 src/core/core.cpp                              |  2 +-
 src/qtui/chatitem.cpp                          |  2 +-
 src/qtui/chatlinemodelitem.cpp                 | 16 ++++++++--------
 src/qtui/chatviewsettings.cpp                  |  2 +-
 src/qtui/mainwin.cpp                           |  2 +-
 src/qtui/settingsdlg.cpp                       |  2 +-
 .../settingspages/chatmonitorsettingspage.cpp  |  2 +-
 .../settingspages/networkssettingspage.cpp     |  2 +-
 src/qtui/settingspages/shortcutsmodel.cpp      |  2 +-
 src/uisupport/networkmodelcontroller.cpp       |  2 +-
 src/uisupport/toolbaractionprovider.cpp        |  2 +-
 21 files changed, 48 insertions(+), 48 deletions(-)

--- a/src/client/coreaccountmodel.cpp
+++ b/src/client/coreaccountmodel.cpp
@@ -107,7 +107,7 @@ QVariant CoreAccountModel::data(const QM
     case Qt::DisplayRole:
         return acc.accountName();
     case AccountIdRole:
-        return QVariant::fromValue<AccountId>(acc.accountId());
+        return QVariant::fromValue(acc.accountId());
     case UuidRole:
         return acc.uuid().toString();
 
--- a/src/client/messagefilter.cpp
+++ b/src/client/messagefilter.cpp
@@ -191,7 +191,7 @@ bool MessageFilter::filterAcceptsRow(int
             if (!redirectedTo.isValid()) {
                 BufferId redirectedTo = Client::bufferModel()->currentIndex().data(NetworkModel::BufferIdRole).value<BufferId>();
                 if (redirectedTo.isValid())
-                    sourceModel()->setData(sourceIdx, QVariant::fromValue<BufferId>(redirectedTo), MessageModel::RedirectedToRole);
+                    sourceModel()->setData(sourceIdx, QVariant::fromValue(redirectedTo), MessageModel::RedirectedToRole);
             }
 
             if (_validBuffers.contains(redirectedTo))
--- a/src/client/messagemodel.cpp
+++ b/src/client/messagemodel.cpp
@@ -448,11 +448,11 @@ QVariant MessageModelItem::data(int colu
 
     switch (role) {
     case MessageModel::MessageRole:
-        return QVariant::fromValue<Message>(message());
+        return QVariant::fromValue(message());
     case MessageModel::MsgIdRole:
-        return QVariant::fromValue<MsgId>(msgId());
+        return QVariant::fromValue(msgId());
     case MessageModel::BufferIdRole:
-        return QVariant::fromValue<BufferId>(bufferId());
+        return QVariant::fromValue(bufferId());
     case MessageModel::TypeRole:
         return msgType();
     case MessageModel::FlagsRole:
@@ -460,9 +460,9 @@ QVariant MessageModelItem::data(int colu
     case MessageModel::TimestampRole:
         return timestamp();
     case MessageModel::RedirectedToRole:
-        return qVariantFromValue<BufferId>(_redirectedTo);
+        return QVariant::fromValue(_redirectedTo);
     default:
-        return QVariant();
+        return {};
     }
 }
 
--- a/src/client/networkmodel.cpp
+++ b/src/client/networkmodel.cpp
@@ -72,7 +72,7 @@ QVariant NetworkItem::data(int column, i
         else
             return QVariant();
     case NetworkModel::NetworkIdRole:
-        return qVariantFromValue(_networkId);
+        return QVariant::fromValue(_networkId);
     case NetworkModel::ItemTypeRole:
         return NetworkModel::NetworkItemType;
     case NetworkModel::ItemActiveRole:
@@ -406,11 +406,11 @@ QVariant BufferItem::data(int column, in
     case NetworkModel::ItemTypeRole:
         return NetworkModel::BufferItemType;
     case NetworkModel::BufferIdRole:
-        return qVariantFromValue(bufferInfo().bufferId());
+        return QVariant::fromValue(bufferInfo().bufferId());
     case NetworkModel::NetworkIdRole:
-        return qVariantFromValue(bufferInfo().networkId());
+        return QVariant::fromValue(bufferInfo().networkId());
     case NetworkModel::BufferInfoRole:
-        return qVariantFromValue(bufferInfo());
+        return QVariant::fromValue(bufferInfo());
     case NetworkModel::BufferTypeRole:
         return int(bufferType());
     case NetworkModel::ItemActiveRole:
@@ -418,9 +418,9 @@ QVariant BufferItem::data(int column, in
     case NetworkModel::BufferActivityRole:
         return (int)activityLevel();
     case NetworkModel::BufferFirstUnreadMsgIdRole:
-        return qVariantFromValue(firstUnreadMsgId());
+        return QVariant::fromValue(firstUnreadMsgId());
     case NetworkModel::MarkerLineMsgIdRole:
-        return qVariantFromValue(markerLineMsgId());
+        return QVariant::fromValue(markerLineMsgId());
     default:
         return PropertyMapItem::data(column, role);
     }
@@ -523,7 +523,7 @@ QVariant QueryBufferItem::data(int colum
     case Qt::EditRole:
         return BufferItem::data(column, Qt::DisplayRole);
     case NetworkModel::IrcUserRole:
-        return QVariant::fromValue<QObject *>(_ircUser);
+        return QVariant::fromValue(_ircUser);
     case NetworkModel::UserAwayRole:
         return (bool)_ircUser ? _ircUser->isAway() : false;
     default:
@@ -750,7 +750,7 @@ QVariant ChannelBufferItem::data(int col
 {
     switch (role) {
     case NetworkModel::IrcChannelRole:
-        return QVariant::fromValue<QObject *>(_ircChannel);
+        return QVariant::fromValue(_ircChannel);
     default:
         return BufferItem::data(column, role);
     }
@@ -1147,7 +1147,7 @@ QVariant IrcUserItem::data(int column, i
     case NetworkModel::IrcChannelRole:
         return parent()->data(column, role);
     case NetworkModel::IrcUserRole:
-        return QVariant::fromValue<QObject *>(_ircUser.data());
+        return QVariant::fromValue(_ircUser.data());
     case NetworkModel::UserAwayRole:
         return (bool)_ircUser ? _ircUser->isAway() : false;
     default:
--- a/src/common/buffersyncer.cpp
+++ b/src/common/buffersyncer.cpp
@@ -90,8 +90,7 @@ QVariantList BufferSyncer::initLastSeenM
     QVariantList list;
     QHash<BufferId, MsgId>::const_iterator iter = _lastSeenMsg.constBegin();
     while (iter != _lastSeenMsg.constEnd()) {
-        list << QVariant::fromValue<BufferId>(iter.key())
-             << QVariant::fromValue<MsgId>(iter.value());
+        list << QVariant::fromValue(iter.key()) << QVariant::fromValue(iter.value());
         ++iter;
     }
     return list;
@@ -113,8 +112,7 @@ QVariantList BufferSyncer::initMarkerLin
     QVariantList list;
     QHash<BufferId, MsgId>::const_iterator iter = _markerLines.constBegin();
     while (iter != _markerLines.constEnd()) {
-        list << QVariant::fromValue<BufferId>(iter.key())
-             << QVariant::fromValue<MsgId>(iter.value());
+        list << QVariant::fromValue(iter.key()) << QVariant::fromValue(iter.value());
         ++iter;
     }
     return list;
@@ -136,8 +134,7 @@ QVariantList BufferSyncer::initActivitie
     QVariantList list;
     auto iter = _bufferActivities.constBegin();
     while (iter != _bufferActivities.constEnd()) {
-        list << QVariant::fromValue<BufferId>(iter.key())
-             << QVariant::fromValue<int>((int) iter.value());
+        list << QVariant::fromValue(iter.key()) << QVariant::fromValue((int)iter.value());
         ++iter;
     }
     return list;
@@ -197,8 +194,7 @@ QVariantList BufferSyncer::initHighlight
     QVariantList list;
     auto iter = _highlightCounts.constBegin();
     while (iter != _highlightCounts.constEnd()) {
-        list << QVariant::fromValue<BufferId>(iter.key())
-             << QVariant::fromValue<int>((int) iter.value());
+        list << QVariant::fromValue(iter.key()) << QVariant::fromValue((int)iter.value());
         ++iter;
     }
     return list;
--- a/src/common/network.cpp
+++ b/src/common/network.cpp
@@ -1209,8 +1209,8 @@ QDataStream &operator<<(QDataStream &out
     i["CodecForServer"]            = info.codecForServer;
     i["CodecForEncoding"]          = info.codecForEncoding;
     i["CodecForDecoding"]          = info.codecForDecoding;
-    i["NetworkId"]                 = QVariant::fromValue<NetworkId>(info.networkId);
-    i["Identity"]                  = QVariant::fromValue<IdentityId>(info.identity);
+    i["NetworkId"]                 = QVariant::fromValue(info.networkId);
+    i["Identity"]                  = QVariant::fromValue(info.identity);
     i["MessageRateBurstSize"]      = info.messageRateBurstSize;
     i["MessageRateDelay"]          = info.messageRateDelay;
     i["AutoReconnectInterval"]     = info.autoReconnectInterval;
--- a/src/common/serializers/serializers.cpp
+++ b/src/common/serializers/serializers.cpp
@@ -29,7 +29,7 @@ bool toVariant(QDataStream& stream, Quas
     if (!Serializers::deserialize(stream, features, content)) {
         return false;
     }
-    data = QVariant::fromValue<T>(content);
+    data = QVariant::fromValue(content);
     return true;
 }
 
--- a/src/common/types.h
+++ b/src/common/types.h
@@ -100,27 +100,27 @@ inline uint qHash(const SignedId64 &id)
 
 struct UserId : public SignedId {
     inline UserId(int _id = 0) : SignedId(_id) {}
-    //inline operator QVariant() const { return QVariant::fromValue<UserId>(*this); }  // no automatic conversion!
+    //inline operator QVariant() const { return QVariant::fromValue(*this); }  // no automatic conversion!
 };
 
 struct MsgId : public SignedId64 {
     inline MsgId(qint64 _id = 0) : SignedId64(_id) {}
-    //inline operator QVariant() const { return QVariant::fromValue<MsgId>(*this); }
+    //inline operator QVariant() const { return QVariant::fromValue(*this); }
 };
 
 struct BufferId : public SignedId {
     inline BufferId(int _id = 0) : SignedId(_id) {}
-    //inline operator QVariant() const { return QVariant::fromValue<BufferId>(*this); }
+    //inline operator QVariant() const { return QVariant::fromValue(*this); }
 };
 
 struct NetworkId : public SignedId {
     inline NetworkId(int _id = 0) : SignedId(_id) {}
-    //inline operator QVariant() const { return QVariant::fromValue<NetworkId>(*this); }
+    //inline operator QVariant() const { return QVariant::fromValue(*this); }
 };
 
 struct IdentityId : public SignedId {
     inline IdentityId(int _id = 0) : SignedId(_id) {}
-    //inline operator QVariant() const { return QVariant::fromValue<IdentityId>(*this); }
+    //inline operator QVariant() const { return QVariant::fromValue(*this); }
 };
 
 struct AccountId : public SignedId {
--- a/src/common/util.h
+++ b/src/common/util.h
@@ -53,7 +53,7 @@ QVariantList toVariantList(const QList<T
 {
     QVariantList variants;
     for (int i = 0; i < list.count(); i++) {
-        variants << QVariant::fromValue<T>(list[i]);
+        variants << QVariant::fromValue(list[i]);
     }
     return variants;
 }
--- a/src/core/core.cpp
+++ b/src/core/core.cpp
@@ -301,7 +301,7 @@ void Core::saveState()
     if (_storage) {
         QVariantList activeSessions;
         for (auto &&user : instance()->_sessions.keys())
-            activeSessions << QVariant::fromValue<UserId>(user);
+            activeSessions << QVariant::fromValue(user);
         _storage->setCoreState(activeSessions);
     }
 }
--- a/src/qtui/chatitem.cpp
+++ b/src/qtui/chatitem.cpp
@@ -871,7 +871,7 @@ void ContentsChatItem::addActionsToMenu(
         case Clickable::Url:
             privateData()->activeClickable = click;
             menu->addAction(icon::get("edit-copy"), tr("Copy Link Address"),
-                &_actionProxy, SLOT(copyLinkToClipboard()))->setData(QVariant::fromValue<void *>(this));
+                            &_actionProxy, SLOT(copyLinkToClipboard()))->setData(QVariant::fromValue(static_cast<void *>(this)));
             break;
         case Clickable::Channel:
         {
--- a/src/qtui/chatlinemodelitem.cpp
+++ b/src/qtui/chatlinemodelitem.cpp
@@ -76,7 +76,7 @@ bool ChatLineModelItem::setData(int colu
 QVariant ChatLineModelItem::data(int column, int role) const
 {
     if (role == ChatLineModel::MsgLabelRole)
-        return QVariant::fromValue<UiStyle::MessageLabel>(messageLabel());
+        return QVariant::fromValue(messageLabel());
 
     QVariant variant;
     MessageModel::ColumnType col = (MessageModel::ColumnType)column;
@@ -111,7 +111,8 @@ QVariant ChatLineModelItem::timestampDat
     case ChatLineModel::SelectedBackgroundRole:
         return backgroundBrush(UiStyle::FormatType::Timestamp, true);
     case ChatLineModel::FormatRole:
-        return QVariant::fromValue<UiStyle::FormatList>({std::make_pair(quint16{0}, UiStyle::Format{UiStyle::formatType(_styledMsg.type()) | UiStyle::FormatType::Timestamp, {}, {}})});
+        return QVariant::fromValue(UiStyle::FormatList{
+            std::make_pair(quint16{0}, UiStyle::Format{UiStyle::formatType(_styledMsg.type()) | UiStyle::FormatType::Timestamp, {}, {}})});
     }
     return QVariant();
 }
@@ -129,7 +130,8 @@ QVariant ChatLineModelItem::senderData(i
     case ChatLineModel::SelectedBackgroundRole:
         return backgroundBrush(UiStyle::FormatType::Sender, true);
     case ChatLineModel::FormatRole:
-        return QVariant::fromValue<UiStyle::FormatList>({std::make_pair(quint16{0}, UiStyle::Format{UiStyle::formatType(_styledMsg.type()) | UiStyle::FormatType::Sender, {}, {}})});
+        return QVariant::fromValue(UiStyle::FormatList{
+            std::make_pair(quint16{0}, UiStyle::Format{UiStyle::formatType(_styledMsg.type()) | UiStyle::FormatType::Sender, {}, {}})});
     }
     return QVariant();
 }
@@ -146,11 +148,11 @@ QVariant ChatLineModelItem::contentsData
     case ChatLineModel::SelectedBackgroundRole:
         return backgroundBrush(UiStyle::FormatType::Contents, true);
     case ChatLineModel::FormatRole:
-        return QVariant::fromValue<UiStyle::FormatList>(_styledMsg.contentsFormatList());
+        return QVariant::fromValue(_styledMsg.contentsFormatList());
     case ChatLineModel::WrapListRole:
         if (_wrapList.isEmpty())
             computeWrapList();
-        return QVariant::fromValue<ChatLineModel::WrapList>(_wrapList);
+        return QVariant::fromValue(_wrapList);
     }
     return QVariant();
 }
@@ -174,7 +176,7 @@ QVariant ChatLineModelItem::backgroundBr
     QTextCharFormat fmt = QtUi::style()->format({UiStyle::formatType(_styledMsg.type()) | subelement, {}, {}},
                                                 messageLabel() | (selected ? UiStyle::MessageLabel::Selected : UiStyle::MessageLabel::None));
     if (fmt.hasProperty(QTextFormat::BackgroundBrush))
-        return QVariant::fromValue<QBrush>(fmt.background());
+        return QVariant::fromValue(fmt.background());
     return QVariant();
 }
 
--- a/src/qtui/mainwin.cpp
+++ b/src/qtui/mainwin.cpp
@@ -1774,7 +1774,7 @@ void MainWin::clientNetworkCreated(Netwo
     const Network *net = Client::network(id);
     QAction *act = new QAction(net->networkName(), this);
     act->setObjectName(QString("NetworkAction-%1").arg(id.toInt()));
-    act->setData(QVariant::fromValue<NetworkId>(id));
+    act->setData(QVariant::fromValue(id));
     connect(net, SIGNAL(updatedRemotely()), this, SLOT(clientNetworkUpdated()));
     connect(act, SIGNAL(triggered()), this, SLOT(connectOrDisconnectFromNet()));
 
--- a/src/qtui/settingsdlg.cpp
+++ b/src/qtui/settingsdlg.cpp
@@ -95,7 +95,7 @@ void SettingsDlg::registerSettingsPage(S
     else
         item = new QTreeWidgetItem(cat, QStringList(sp->title()));
 
-    item->setData(0, SettingsPageRole, QVariant::fromValue<QObject *>(sp));
+    item->setData(0, SettingsPageRole, QVariant::fromValue(sp));
     pageIsLoaded[sp] = false;
     if (!ui.settingsTree->selectedItems().count())
         ui.settingsTree->setCurrentItem(item);
--- a/src/qtui/settingspages/chatmonitorsettingspage.cpp
+++ b/src/qtui/settingspages/chatmonitorsettingspage.cpp
@@ -161,8 +161,8 @@ void ChatMonitorSettingsPage::save()
 
     // save list of active buffers
     QVariantList saveableBufferIdList;
-    foreach(BufferId id, _configActive->bufferList()) {
-        saveableBufferIdList << QVariant::fromValue<BufferId>(id);
+    foreach (BufferId id, _configActive->bufferList()) {
+        saveableBufferIdList << QVariant::fromValue(id);
     }
 
     chatViewSettings.setValue("Buffers", saveableBufferIdList);
--- a/src/qtui/settingspages/networkssettingspage.cpp
+++ b/src/qtui/settingspages/networkssettingspage.cpp
@@ -570,7 +570,7 @@ QListWidgetItem *NetworksSettingsPage::i
         }
         if (!item) item = new QListWidgetItem(disconnectedIcon, info.networkName, ui.networkList);
     }
-    item->setData(Qt::UserRole, QVariant::fromValue<NetworkId>(info.networkId));
+    item->setData(Qt::UserRole, QVariant::fromValue(info.networkId));
     setItemState(info.networkId, item);
     widgetHasChanged();
     return item;
--- a/src/qtui/settingspages/shortcutsmodel.cpp
+++ b/src/qtui/settingspages/shortcutsmodel.cpp
@@ -156,7 +156,7 @@ QVariant ShortcutsModel::data(const QMod
         return QVariant();
 
     case ActionRole:
-        return QVariant::fromValue<QObject *>(action);
+        return QVariant::fromValue(action);
 
     case DefaultShortcutRole:
         return action->shortcut(Action::DefaultShortcut);
--- a/src/uisupport/networkmodelcontroller.cpp
+++ b/src/uisupport/networkmodelcontroller.cpp
@@ -577,7 +577,7 @@ NetworkModelController::JoinDlg::JoinDlg
     foreach(NetworkId id, Client::networkIds()) {
         const Network *net = Client::network(id);
         if (net->isConnected()) {
-            networks->addItem(net->networkName(), QVariant::fromValue<NetworkId>(id));
+            networks->addItem(net->networkName(), QVariant::fromValue(id));
         }
     }
 
--- a/src/uisupport/toolbaractionprovider.cpp
+++ b/src/uisupport/toolbaractionprovider.cpp
@@ -165,7 +165,7 @@ void ToolBarActionProvider::networkCreat
     Action *act = new Action(net->networkName(), this);
     _networkActions[id] = act;
     act->setObjectName(QString("NetworkAction-%1").arg(id.toInt()));
-    act->setData(QVariant::fromValue<NetworkId>(id));
+    act->setData(QVariant::fromValue(id));
     connect(net, SIGNAL(updatedRemotely()), SLOT(networkUpdated()));
     connect(act, SIGNAL(triggered()), SLOT(connectOrDisconnectNet()));
     networkUpdated(net);
